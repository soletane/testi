<!DOCTYPE html>
<html>
<head>
<title>Ristinolla</title>
<style>
.board {	font-family: Arial;
        	font-size: 50px;
            font-weight: bold;
        	text-align: center;
        	top-margin: 50px;
            letter-spacing: 3px;
}
p	{		font-family: Arial;
        	font-size: 20px;
        	text-align: center;
        	top-margin: 50px;
            letter-spacing: 3px;
}
</style>
</head>
<body>
<p class="board">
<script type="text/javascript">


let board = [0,0,0,0,0,0,0,0,0];
let vuoro = 1;
function drawBoard(board) {
  for (y=0; y<3; y++){
    document.write('<br>');
    for (let x=0; x<3; x++){
      document.write(board[x+(y*3)]);
      
    }
    
  }
};
function tryMove(board) {
	let userInput = document.getElementById('userInput').value;
    
    if (userInput >= 1 && userInput <=9) {
    	document.getElementById('palaute').innerHTML = "Annoit sopivan luvun!";
        if (board[userInput-1] === vuoro){
        	document.getElementById('palaute').innerHTML = "Ruutu on täytetty!";
        } else {
        	board[userInput-1] = vuoro;
            
          }        
    } else {
    document.getElementById('palaute').innerHTML = "Annoit epäkelvon luvun!";
    };
    
};
/*function makeAMove(int siirto, int vuoro) {
    while 1 {
      if (board[siirto] === vuoro){
          document.getElementById('palaute').innerHTML = "Se ruutu on jo sinun täyttämä. Valitse toinen ruutu.";
      };
	};
    board[siirto] = vuoro;
    
};*/	
do {
drawBoard(board);
tryMove(board);
} while (vuoro<3);
</script>
</p>
<p>Mihin haluat laittaa seuraavan merkin?<p>
<input type='text' id='userInput' value='Valitse numero väliltä 1-9' />
<input type='button' onclick='tryMove()' value='Tee siirto'/>
<p id="palaute">Palaute</p>
</body>
</html>
